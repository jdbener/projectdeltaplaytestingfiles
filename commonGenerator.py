import urllib.request, csv, re, random

print("Enter the ids/urls of the sheets:")

def getURLs():
    out = []
    while True:
        tmp = input("Next sheet: ")
        if tmp == "": return out
        out.append(tmp.strip())

def numberToGenerate():
    while True:
        try:
            tmp = input("How many cards should I generate? ")
            return int(tmp);
        except: print("Not a number!", sufix=" ")

def getColorMask():
    tmp = input("Any colors to look for (all)? ")
    if len(tmp) == 0: tmp = "r, o, y, g, b, p"
    tmp = tmp.upper()

    return [x.strip() for x in tmp.split(",")]

def getTypeMask():
    tmp = input("What types (all)? ")
    if len(tmp) == 0: tmp = "asset, generator, effect"

    return [x.strip().title() for x in tmp.split(",")]


urls = getURLs()
db = {}
for sheet in urls:
    sheet = sheet.replace("https://docs.google.com/spreadsheets/d/","").split("/")[0]
    sheet = urllib.request.urlopen("https://docs.google.com/spreadsheets/d/" + sheet + "/gviz/tq?tqx=out:csv")\
        .read().decode('utf-8').replace(" (0-4 = common, 5-8 = uncommon, 9-10 = rare)", "")

    reader = csv.DictReader(sheet.splitlines())
    for card in reader:
        if len(card['Setted Slot']) and card['Setted Slot'] != '0':
            try:
                # Confirm that the card hasn't already been processed
                if not (card['Name'] in db):
                    if not (card['Name'] == 'R' or card['Name'] == 'U' or card['Name'] == 'C' or len(card['Name']) == 0):
                        db[card['Name']] = card
                        colors = [x[0] if len(x) > 0 else x for x in card['Color Calculator'].upper().split(" ")]
                        colors += card['Color'].upper().replace("GENERIC", " ")[0]
                        colors = list(set(colors))
                        colors.sort()
                        db[card['Name']]["Colors"] = colors

                        db[card['Name']]["Better Than Common"] = int(card['Effectiveness']) >= 5;
                else:
                    print("card: ", card['Name'], " already found in card database... skipping.")
            except:
                print("card parsing failed.")
                raise

negatives = ["Higher cost", "Reduced effectiveness", "Costs loyalty/deals damage to you", "Must forfit something", "Group Hug", "Effect costs energy", "Underpowered Statline", "Warrent", "Doubt", "Time Delay", "Must be 'Prepare'ed", "Color's negative keyword", "Has Once"]
for i in range(0, 5): negatives.append("No Negative")

colorMask = getColorMask()
typeMask = getTypeMask()
possibleEffects = []
for cardName in db:
    card = db[cardName]
    if not card["Better Than Common"]: continue
    if not any(i in colorMask for i in card["Colors"]): continue
    # if not any(i in typeMask for i in card["Type"]): continue

    possibleEffects.extend(card["Rules"].split("<p>"))

# print(possibleEffects)

number = numberToGenerate()
for i in range(0, number):
    negs = random.sample(negatives, 2)
    effect = random.sample(possibleEffects, 1)

    print("Card which does: ", effect[0], " but it has ", negs[0], " and ", negs[1])
    pass
