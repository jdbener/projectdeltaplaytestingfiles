import urllib.request, csv, re, untangle, random, math

print("Enter the ids/urls of the sheets:")

def getURLs():
    out = []
    while True:
        tmp = input("Next sheet: ")
        if tmp == "": return out
        out.append(tmp.strip())
    return out

def getPoolSize():
    print("Please enter the pool size: ")
    while True:
        try:
            tmp = input()
            return int(tmp)
        except:
            print("Invalid number entered... Please enter a number: ")
    return 0

urls = getURLs()
db = {}
print("Building database")
for sheet in urls:
    sheet = sheet.replace("https://docs.google.com/spreadsheets/d/","").split("/")[0]
    sheet = urllib.request.urlopen("https://docs.google.com/spreadsheets/d/" + sheet + "/gviz/tq?tqx=out:csv")\
        .read().decode('utf-8').replace(" (0-4 = common, 5-8 = uncommon, 9-10 = rare)", "")

    reader = csv.DictReader(sheet.splitlines())
    for card in reader:
        if len(card['Setted Slot']):
            try:
                if not (card['Name'] == 'R' or card['Name'] == 'U' or card['Name'] == 'C'):
                    card['Name'] = card['Name'].replace(",", "")
                    if not card['Name'] in db: db[card['Name']] = card
            except:
                print("card parsing failed.")
                continue


commonPool = []
uncommonPool = []
rarePool = []
for card in db.values():
    try:
        effect = int(card['Effectiveness'])
        for x in range(0, 11 - effect): # If a card has an effectiveness of 1 add 10 copies to the pool, if a card has an effectiveness of 10 add one copy to the pool
            if 1 <= effect < 5: commonPool.append(card['Name'])
            elif 5 <= effect < 9: uncommonPool.append(card['Name'])
            elif 9 <= effect <= 10:
                rarePool.append(card['Name'])
    except: pass


# Determine how many cards we need to widdle the pool down to
poolSize = getPoolSize();

# Remove random elements from the array until we have widdled down to the desired pool size
while(len(commonPool) > poolSize/2): del commonPool[random.randrange(0, len(commonPool))]
while(len(uncommonPool) > poolSize/3): del uncommonPool[random.randrange(0, len(uncommonPool))]
while(len(rarePool) > poolSize/6): del rarePool[random.randrange(0, len(rarePool))]

# Print out the deck
print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<cockatrice_deck version=\"1\">\n<deckname>Draft Pool</deckname>\n<comments>To use this pool, strip cards into the sideboard (double click on them) until your deck is the correct size for the draft you are participating in.</comments>\n<zone name=\"main\">")
for card in commonPool + uncommonPool + rarePool:
    print("\t<card number=\"1\" name=\"" + card + "\"/>\n")
print("</zone>\n</cockatrice_deck>")
# print(commonPool)
# print(uncommonPool)
# print(rarePool)
# print(len(pool))
